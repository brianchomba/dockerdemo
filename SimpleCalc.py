# Program make a simple calculator

# This function adds two numbers
def add(x, y):
    return x + y

# This function subtracts two numbers
def subtract(x, y):
    return x - y

# This function multiplies two numbers
def multiply(x, y):
    return x * y

# This function divides two numbers
def divide(x, y):
    return x / y


print("Sum 25 + 5 = ", add(25, 5))
print("Difference 25 - 5 = ", subtract(25,5))
print("Multiplication 25 * 5 = ", multiply(25, 5))
print("Division 25 / 5 = ", divide(25, 5))

print("\nThis my simple docker calculator container")

exit(0)
